package com.myprojects.articlesapp.ui.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.myprojects.articlesapp.R


class CategoryView(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    CardView(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    private var pressedColor: Int = 0
    private val backColor = resources.getColor(R.color.categoryBack, null)
    private val  root = LayoutInflater.from(context).inflate(R.layout.category_view_layout, this, true)
    private var textView = root.findViewById<TextView>(R.id.text)

    init {
        if(attrs != null) {
            val a = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.CategoryView,
                0, 0
            )

            try {
                pressedColor = a.getColor(R.styleable.CategoryView_pressedColor, 0)
                textView.text = a.getString(R.styleable.CategoryView_text)
            } finally {
                a.recycle()
            }
        }
    }

     fun setAsSelected(selected: Boolean) {
         isSelected = selected
        if(selected) textView.setBackgroundColor(pressedColor) else textView.setBackgroundColor(backColor)
    }
}
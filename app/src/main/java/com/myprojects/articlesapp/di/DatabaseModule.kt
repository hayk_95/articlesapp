package com.myprojects.articlesapp.di

import android.content.Context
import androidx.room.Room
import com.myprojects.articlesapp.data.db.AppDatabase
import com.myprojects.articlesapp.data.db.dao.ArticleDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ContextModule::class])
class DatabaseModule {

    @Singleton
    @Provides
    fun provideArticleDao(context: Context) : ArticleDao{
        return Room.databaseBuilder(context,AppDatabase::class.java, "database").build().articleDao()
    }
}
package com.myprojects.articlesapp.ui.destination.articles

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.myprojects.articlesapp.model.StatusResource
import com.myprojects.articlesapp.data.repository.ArticlesRepository
import com.myprojects.articlesapp.model.Article
import com.myprojects.articlesapp.model.Category
import com.myprojects.articlesapp.ui.AppDelegate
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class ArticlesViewModel(application: Application):  AndroidViewModel(application) {
    private val disposable = CompositeDisposable()
    private val articlesRepository: ArticlesRepository = (application as AppDelegate).appComponent.getArticlesRepository()
    lateinit var currentCategory: Category
    val articlesLiveData: MutableLiveData<PagedList<Article>> = MutableLiveData()
    var listPosition: Int = 0

    init {
        changeCategory(Category.SCHRANG)
    }

    fun changeCategory(category: Category) {
        disposable.clear()
        currentCategory = category
        disposable.add(articlesRepository.getArticlesList(category)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
            articlesLiveData.postValue(it)
        })
    }

    fun getStatusData() : MutableLiveData<StatusResource>{
        return articlesRepository.statusResource
    }
}
package com.myprojects.articlesapp.ui.destination.articlesinfo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.TextView
import com.myprojects.articlesapp.R

class ArticleInfoFragment : Fragment(R.layout.fragment_article_info) {
    private var title: String? = null
    private var content: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            title = it.getString(TITLE_ARG)
            content = it.getString(CONTENT_ARG)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<TextView>(R.id.title).text = title
        view.findViewById<TextView>(R.id.content).text = content
    }

    companion object {
        const val TITLE_ARG = "title"
        const val CONTENT_ARG = "content"
    }
}
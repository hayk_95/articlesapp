package com.myprojects.articlesapp.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
package com.myprojects.articlesapp.data.api

import com.myprojects.articlesapp.data.api.response.ArticlesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("applications/app/content/list?per_page=10&order=id")
    fun getArticles(@Query("category") category: Int, @Query("page") page: Int) : Observable<ArticlesResponse>
}
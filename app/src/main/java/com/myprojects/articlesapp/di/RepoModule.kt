package com.myprojects.articlesapp.di

import com.myprojects.articlesapp.data.api.ApiService
import com.myprojects.articlesapp.data.db.dao.ArticleDao
import com.myprojects.articlesapp.data.repository.ArticlesRepository
import dagger.Module
import dagger.Provides

@Module(includes = [DatabaseModule::class, NetworkModule::class])
class RepoModule {

    @Provides
    fun getArticleRepository(apiService: ApiService, articleDao: ArticleDao) : ArticlesRepository{
        return ArticlesRepository(apiService, articleDao)
    }
}
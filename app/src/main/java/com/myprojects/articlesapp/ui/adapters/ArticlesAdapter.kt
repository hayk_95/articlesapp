package com.myprojects.articlesapp.ui.adapters

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.Group
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.myprojects.articlesapp.R
import com.myprojects.articlesapp.model.Article
import com.myprojects.articlesapp.model.Category
import com.myprojects.articlesapp.ui.customview.CategoryView


private const val CATEGORY_TYPE = 0
private const val ARTICLE_TYPE = 1

class ArticlesAdapter(var currentCategory: Category) :
    PagedListAdapter<Article, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    private var listener: AdapterItemsClickListener? = null

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) CATEGORY_TYPE else ARTICLE_TYPE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == CATEGORY_TYPE)
            CategoryHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.category_holder_layout, parent, false)
            )
        else ArticleHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.article_holder_layout, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return if (super.getItemCount() == 0) 1 else super.getItemCount()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ArticleHolder) {
            getItem(position - 1)?.let { holder.bindData(it) }
        } else {
            (holder as CategoryHolder).initUI()
        }
    }

    fun updateData(data: PagedList<Article>) {
        submitList(data)
    }

    interface AdapterItemsClickListener {
        fun onCategoryClicked(category: Category)
        fun onArticleClicked(article: Article)
    }

    fun setOnAdapterItemsClickListener(listener: AdapterItemsClickListener) {
        this.listener = listener
    }

    inner class CategoryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun initUI() {
            for (i in 0 until (itemView as LinearLayout).childCount) {
                itemView.getChildAt(i).setOnClickListener {
                    if (!it.isSelected) {
                        for (j in 0 until itemView.childCount) {
                            (itemView.getChildAt(j) as CategoryView).setAsSelected(false)
                        }
                        (it as CategoryView).setAsSelected(true)
                        currentCategory = Category.values()[i]
                        listener?.onCategoryClicked(Category.values()[i])
                    }
                }
            }
            setSelected(itemView)
        }

        private fun setSelected(itemView: LinearLayout){
            for (j in 0 until itemView.childCount) {
                (itemView.getChildAt(j) as CategoryView).setAsSelected(false)
            }
            (itemView.getChildAt(currentCategory.num - 1) as CategoryView).setAsSelected(true)
        }
    }

    inner class ArticleHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.title)
        var image: ImageView = itemView.findViewById(R.id.picture)
        var date: TextView = itemView.findViewById(R.id.date)
        var group: Group = itemView.findViewById(R.id.group)

        fun bindData(article: Article) {
            title.text = article.title
            date.text = article.date
            group.visibility = View.GONE
            Glide.with(itemView).load(article.image).placeholder(R.drawable.placeholder)
                .addListener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        group.visibility = View.VISIBLE
                        return false
                    }
                }).into(image)

            itemView.setOnClickListener{
                listener?.onArticleClicked(article)
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object :
            DiffUtil.ItemCallback<Article>() {

            override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
                return oldItem == newItem
            }
        }
    }
}
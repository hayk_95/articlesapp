package com.myprojects.articlesapp.model

enum class Category(val num: Int) {
    SCHRANG(1), TALK(2), SPIRIT(3);
}
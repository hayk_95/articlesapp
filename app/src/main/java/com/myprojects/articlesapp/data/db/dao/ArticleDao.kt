package com.myprojects.articlesapp.data.db.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.myprojects.articlesapp.model.Article

@Dao
interface ArticleDao {

    @Query("SELECT * FROM article WHERE category = :artCategory ORDER BY id")
    fun getArticles(artCategory: Int): DataSource.Factory<Int, Article>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveArticles(list: List<Article>)

    @Query("SELECT COUNT(id) FROM Article WHERE category = :artCategory")
    fun getArticlesCount(artCategory: Int) : Int

    @Query("DELETE FROM article WHERE category = :artCategory")
    fun removeArticles(artCategory: Int)

}
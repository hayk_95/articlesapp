package com.myprojects.articlesapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Article(
    @PrimaryKey
    val id: Int,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "content")
    val content: String,
    @ColumnInfo(name = "image")
    val image: String,
    @ColumnInfo(name = "date_created")
    val date: String,
    @ColumnInfo(name = "category")
    var category: Int = 0
)
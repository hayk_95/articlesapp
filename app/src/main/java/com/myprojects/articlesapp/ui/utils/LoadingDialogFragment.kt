package com.myprojects.articlesapp.ui.utils

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import com.myprojects.articlesapp.R

class LoadingDialogFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_loading_dialog, container, false)
    }

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            val window: Window? = dialog?.window
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val windowParams: WindowManager.LayoutParams? = window?.attributes
            windowParams?.dimAmount = 0.5f
            windowParams?.flags = windowParams?.flags?.or(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            window?.attributes = windowParams
        }
    }
}
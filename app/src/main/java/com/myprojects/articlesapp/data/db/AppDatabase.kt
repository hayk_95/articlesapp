package com.myprojects.articlesapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.myprojects.articlesapp.data.db.dao.ArticleDao
import com.myprojects.articlesapp.model.Article

@Database(entities = [Article::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun articleDao(): ArticleDao
}
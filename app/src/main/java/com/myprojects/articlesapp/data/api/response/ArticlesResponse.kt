package com.myprojects.articlesapp.data.api.response

import com.google.gson.annotations.SerializedName

data class ArticlesResponse(
    val articles: ArrayList<SingleArticleResponse>?,
    @SerializedName("pages")
    val pageInfo: PageInfo
){
    data class PageInfo(@SerializedName("articles") val count: Int,
    @SerializedName("last") val lastIndex: Int)
}
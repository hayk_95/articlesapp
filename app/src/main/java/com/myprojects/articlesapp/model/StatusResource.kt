package com.myprojects.articlesapp.model

class StatusResource private constructor(
    val status: Status,
    val message: String?
) {

    companion object {
        fun success(): StatusResource {
            return StatusResource(
                Status.SUCCESS,
                null
            )
        }

        fun error(msg: String? = null): StatusResource {
            return StatusResource(
                Status.ERROR,
                msg
            )
        }

        fun loading(): StatusResource {
            return StatusResource(
                Status.LOADING,
                null
            )
        }
    }
}
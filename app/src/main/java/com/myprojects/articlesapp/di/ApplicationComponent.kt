package com.myprojects.articlesapp.di

import com.myprojects.articlesapp.data.repository.ArticlesRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, ContextModule::class, DatabaseModule::class, RepoModule::class])
interface ApplicationComponent {
    fun getArticlesRepository() : ArticlesRepository
}
package com.myprojects.articlesapp.data.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import com.myprojects.articlesapp.model.StatusResource
import com.myprojects.articlesapp.data.api.ApiService
import com.myprojects.articlesapp.data.db.dao.ArticleDao
import com.myprojects.articlesapp.model.Article
import com.myprojects.articlesapp.model.Category
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

private const val PAGE_SIZE = 10

class ArticlesRepository @Inject constructor(
    private val apiService: ApiService,
    private val articleDao: ArticleDao
) {

    private lateinit var currentCategory: Category
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val statusResource: MutableLiveData<StatusResource> = MutableLiveData()
    private val pagedListConfig = PagedList.Config.Builder()
        .setInitialLoadSizeHint(PAGE_SIZE)
        .setPrefetchDistance(2)
        .setEnablePlaceholders(false)
        .setPageSize(PAGE_SIZE).build()

    fun getArticlesList(category: Category): Observable<PagedList<Article>> {
        currentCategory = category
        compositeDisposable.clear()

        loadArticles(currentCategory, 0)
        return RxPagedListBuilder(
            articleDao.getArticles(category.num),
            pagedListConfig
        ).setBoundaryCallback(UserBoundaryCallback()).buildObservable()
    }

    fun loadArticles(category: Category, page: Int) {
        compositeDisposable.add(
            apiService.getArticles(category.num, page)
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (it.articles != null && it.articles.isNotEmpty()) {
                        val list = ArrayList<Article>()
                        for (article in it.articles) {
                            with(article){
                                list.add(Article(id, title, content, picture, date, category.num))
                            }
                        }
                        articleDao.saveArticles(list)
                        statusResource.postValue(StatusResource.success())
                    }
                },
                    {
                        statusResource.postValue(StatusResource.error(it.message))
                    })
        )
    }

    inner class UserBoundaryCallback : PagedList.BoundaryCallback<Article?>() {

        override fun onZeroItemsLoaded() {
            statusResource.postValue(StatusResource.loading())
            loadArticles(currentCategory, 0)
        }

        override fun onItemAtFrontLoaded(article: Article) {
        }

        override fun onItemAtEndLoaded(article: Article) {
            compositeDisposable.add(
                Single.create<Int> {
                    it.onSuccess(articleDao.getArticlesCount(currentCategory.num))
                }.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe{count -> loadArticles(currentCategory, (count + PAGE_SIZE - 1) / PAGE_SIZE)}
            )
        }
    }
}
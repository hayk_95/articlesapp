package com.myprojects.articlesapp.ui

import android.app.Application
import com.myprojects.articlesapp.di.ApplicationComponent
import com.myprojects.articlesapp.di.ContextModule
import com.myprojects.articlesapp.di.DaggerApplicationComponent

class AppDelegate : Application() {

    val appComponent: ApplicationComponent = DaggerApplicationComponent.builder().contextModule(ContextModule(this)).build()
}
package com.myprojects.articlesapp.ui.destination.articles

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler
import com.myprojects.articlesapp.R
import com.myprojects.articlesapp.model.Status
import com.myprojects.articlesapp.model.Article
import com.myprojects.articlesapp.model.Category
import com.myprojects.articlesapp.ui.adapters.ArticlesAdapter
import com.myprojects.articlesapp.ui.destination.articlesinfo.ArticleInfoFragment
import com.myprojects.articlesapp.ui.utils.LoadingDialogFragment

class ArticlesFragment : Fragment(R.layout.fragment_articles) {

    private lateinit var articlesList: RecyclerView
    private lateinit var adapter: ArticlesAdapter

    private val loadingTag = "loading"
    private var loadingDialog: LoadingDialogFragment = LoadingDialogFragment()


    val articlesViewModel: ArticlesViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI(view)
        bindViewModelData()
    }

    override fun onPause() {
        super.onPause()
        articlesViewModel.listPosition =
            (articlesList.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
    }

    private fun initUI(view: View) {
        articlesList = view.findViewById(R.id.articles_list)
        articlesList.layoutManager = WrapContentLinearLayoutManager(requireContext())
        adapter = ArticlesAdapter(articlesViewModel.currentCategory)
        articlesList.adapter = adapter

        adapter.setOnAdapterItemsClickListener(object : ArticlesAdapter.AdapterItemsClickListener {
            override fun onCategoryClicked(category: Category) {
                articlesViewModel.changeCategory(category)
            }

            override fun onArticleClicked(article: Article) {
                val bundle = bundleOf(
                    ArticleInfoFragment.TITLE_ARG to article.title,
                    ArticleInfoFragment.CONTENT_ARG to article.content
                )
                Navigation.findNavController(view)
                    .navigate(R.id.action_articlesFragment_to_articleInfoFragment, bundle)
            }
        })

        (articlesList.layoutManager as LinearLayoutManager).scrollToPosition(articlesViewModel.listPosition)
    }

    private fun bindViewModelData() {
        articlesViewModel.articlesLiveData.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                adapter.updateData(it)
            }
        })

        articlesViewModel.getStatusData().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    dismissCurrentDialog()
                }
                Status.ERROR -> {
                    showErrorDialog(it.message)
                }
                Status.LOADING -> {
                    showLoadingDialog()
                }
            }
        })
    }


    private fun showErrorDialog(errorMsg: String?) {
        dismissCurrentDialog()
        val errorMessage =
            if (errorMsg != null && errorMsg.isNotEmpty()) errorMsg
            else getString(R.string.default_error_message)
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.error)
            .setMessage(errorMessage)
            .setPositiveButton(getString(R.string.ok)) { dialog, which -> dialog.dismiss() }
            .create().show()
    }

    private fun showLoadingDialog() {
        if (!loadingDialog.isAdded) {
            loadingDialog.show(childFragmentManager, loadingTag)
        }
    }

    private fun dismissCurrentDialog() {
        if (childFragmentManager.findFragmentByTag(loadingTag) != null)
            loadingDialog.dismiss()
    }

    class WrapContentLinearLayoutManager(context: Context) : LinearLayoutManager(context) {
        override fun onLayoutChildren(
            recycler: Recycler,
            state: RecyclerView.State
        ) {
            try {
                super.onLayoutChildren(recycler, state)
            } catch (e: IndexOutOfBoundsException) {
                Log.e("DBG", e.message ?: "")
            }
        }
    }
}
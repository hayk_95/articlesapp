package com.myprojects.articlesapp.data.api.response

import com.google.gson.annotations.SerializedName

data class SingleArticleResponse(
    val id: Int,
    val title: String,
    val content: String,
    val picture: String,
    @SerializedName("date_created")
    val date: String
)